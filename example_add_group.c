#include "kthp.h"
#include <stdio.h>

#define NUM_THREADS 4

kth_pool_t tp;

void
my_func(void *args)
{
    printf("%d\n", *(int*)args);
}

int
main()
{
    if (kth_pool_init(&tp, NUM_THREADS, 64))
        return 1;

    if (kth_pool_run(&tp))
        return 2;

    int         args[8] = {7, 6, 5, 4, 3, 2, 1, 0};
    kth_job_t   jobs[8];

    for (int i = 0; i < 8; ++i)
    {
        jobs[i].func = my_func;
        jobs[i].args = &args[i];
    }

    kth_job_grp_t grp = kth_create_job_grp(jobs, 8);
    kth_job_grp_run_and_wait(&grp, &tp);

    kth_pool_shutdown(&tp, 0);
    kth_pool_destroy(&tp);

    return 0;
}
