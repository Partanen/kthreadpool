#include "kthp.h"
#include <stdio.h>

#define NUM_THREADS 4

kth_pool_t tp;

void
my_func(void *args)
{
    printf("%d\n", *(int*)args);
}

int
main()
{
    if (kth_pool_init(&tp, NUM_THREADS, 64))
        return 1;

    if (kth_pool_run(&tp))
        return 2;

    int args[8] = {7, 6, 5, 4, 3, 2, 1, 0};

    for (int i = 0; i < 8; ++i)
    {
        kth_pool_add_job(&tp, my_func, &args[i]);
    }

    kth_pool_shutdown(&tp, 1);
    kth_pool_destroy(&tp);

    return 0;
}
