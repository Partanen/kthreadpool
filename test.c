#include "kthp.h"
#include <stdio.h>

static kth_pool_t   tp;
static int32_t      counter = 0;

static void    job_a(void *args);
static void    job_b(void *args);
static void    job_c(void *args);
static int32_t interlocked_increment_int32(int32_t volatile *target);

int main(int argc, char **argv)
{
    if (kth_pool_init(&tp, 4, 4) != 0)
        return 1;

    if (kth_pool_run(&tp) != 0)
        return 2;

    kth_job_t jobs[16];

    for (long long unsigned int i = 0; i < 16; ++i)
    {
        jobs[i].func = job_c;
        jobs[i].args = (void*)i;
    }

    kth_job_grp_t grp = kth_create_job_grp(jobs, 16);
    kth_pool_add_grp(&tp, &grp);

    for (int i = 0; i < 8; ++i)
        kth_pool_add_job(&tp, job_b, (void*)(long long unsigned)i);

    kth_job_grp_wait(&grp, &tp);

    if (kth_pool_shutdown(&tp, 1) != 0)
        return 3;

    if (kth_pool_destroy(&tp) != 0)
        return 4;

    return 0;
}

static void
job_a(void *args)
{
    printf("%s: counter: %d\n", __func__,
        interlocked_increment_int32(&counter) - 1);
    printf("%s: %llu\n", __func__, (long long unsigned)args);
}

static void
job_b(void *args)
{
    kth_pool_add_job(&tp, job_a, args);
}

static void
job_c(void *args)
{
    printf("%s: %llu\n", __func__, (long long unsigned)args);
}


#if defined(__unix__)

#include <unistd.h>

static int32_t
interlocked_increment_int32(int32_t volatile *target)
    {return __sync_add_and_fetch(target, 1);}
#elif defined(_WIN32)

#define WIN32_LEAN_AND_MEAN
#include <windows.h>

static int32_t
interlocked_increment_int32(int32_t volatile *target)
    {return InterlockedIncrement(target);}

#endif /* __unix __ */
