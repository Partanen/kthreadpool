#ifndef KTHP_H
#define KTHP_H

#include <stdint.h>

#define KTHP_SYNC_VARS_BUF_SIZE 128

typedef struct kth_job_t        kth_job_t;
typedef struct kth_job_grp_t    kth_job_grp_t;
typedef struct kth_work_entry_t kth_work_entry_t;
typedef struct kth_pool_t       kth_pool_t;

struct kth_job_t
{
    void (*func)(void*);
    void *args;
};

struct kth_job_grp_t
{
    kth_job_t   *jobs;
    int32_t     num;
    int32_t     num_started;
    int32_t     num_completed;
    int32_t     index;
};

struct kth_pool_t
{
    int                 running;
    void                *threads;
    int                 threads_max;
    int                 threads_num;
    kth_work_entry_t    *work;
    int                 work_max;
    int                 work_first;
    int                 work_num_unstarted;
    char                sync_vars[KTHP_SYNC_VARS_BUF_SIZE];
    char                flags;
};

/* With all functions, a return value of 0 indicates success unless otherwise
 * stated. */

int
kth_pool_init(kth_pool_t *tp, int num_threads, int num_initial_work_slots);

int
kth_pool_run(kth_pool_t *tp);

int
kth_pool_shutdown(kth_pool_t *tp, int graceful);
/* If graceful is non-zero, threads will first complete all pending work. */

int
kth_pool_destroy(kth_pool_t *tp);

int
kth_pool_add_job(kth_pool_t *tp, void (*func)(void*), void *args);

int
kth_pool_add_grp(kth_pool_t *tp, kth_job_grp_t *grp);

int
kth_pool_do_work(kth_pool_t *tp);
/* Returns 1 if work was done, 0 otherwise */

int
kth_job_grp_do_work(kth_job_grp_t *grp, kth_pool_t *tp);
/* Returns 1 if work was done, 0 otherwise */

kth_job_grp_t
kth_create_job_grp(kth_job_t *jobs, int num);

int
kth_job_grp_wait(kth_job_grp_t *grp, kth_pool_t *tp);

int
kth_job_grp_run_and_wait(kth_job_grp_t *grp, kth_pool_t *tp);

#define kth_job_grp_is_completed(grp_) (grp->num_completed >= grp->num)

#endif /* KTHP_H */
